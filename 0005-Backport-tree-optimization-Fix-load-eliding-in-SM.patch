From dc238e97a75835231939e77e8568ccd9bc5187d5 Mon Sep 17 00:00:00 2001
From: zhanghaijian <z.zhanghaijian@huawei.com>
Date: Mon, 12 Jul 2021 10:46:16 +0800
Subject: [PATCH 05/13] [Backport]tree-optimization: Fix load eliding in SM

Reference: https://gcc.gnu.org/git/?p=gcc.git;a=commit;h=0424a5ece5307cc22bbc0fe97edf4707d7a798ed

This fixes the case of not using the multithreaded model when
only conditionally storing to the destination.  We cannot elide
the load in this case.

diff --git a/gcc/testsuite/gcc.dg/torture/pr94949.c b/gcc/testsuite/gcc.dg/torture/pr94949.c
new file mode 100755
index 00000000000..6182d77b3cd
--- /dev/null
+++ b/gcc/testsuite/gcc.dg/torture/pr94949.c
@@ -0,0 +1,17 @@
+/* { dg-do run } */
+/* { dg-additional-options "-fallow-store-data-races" } */
+
+static int x = 1;
+static volatile int y = -1;
+int
+main()
+{
+  for (int i = 0; i < 128; ++i)
+    {
+      if (i == y)
+	x = i;
+    }
+  if (x != 1)
+    __builtin_abort ();
+  return 0;
+}
diff --git a/gcc/tree-ssa-loop-im.c b/gcc/tree-ssa-loop-im.c
index b3fd1647fbd..8c33735b1fa 100644
--- a/gcc/tree-ssa-loop-im.c
+++ b/gcc/tree-ssa-loop-im.c
@@ -2128,9 +2128,9 @@ execute_sm (class loop *loop, vec<edge> exits, im_mem_ref *ref)
   fmt_data.orig_loop = loop;
   for_each_index (&ref->mem.ref, force_move_till, &fmt_data);
 
+  bool always_stored = ref_always_accessed_p (loop, ref, true);
   if (bb_in_transaction (loop_preheader_edge (loop)->src)
-      || (! flag_store_data_races
-	  && ! ref_always_accessed_p (loop, ref, true)))
+      || (! flag_store_data_races && ! always_stored))
     multi_threaded_model_p = true;
 
   if (multi_threaded_model_p)
@@ -2145,8 +2145,10 @@ execute_sm (class loop *loop, vec<edge> exits, im_mem_ref *ref)
 
   /* Avoid doing a load if there was no load of the ref in the loop.
      Esp. when the ref is not always stored we cannot optimize it
-     away later.  */
-  if (ref->loaded && bitmap_bit_p (ref->loaded, loop->num))
+     away later.  But when it is not always stored we must use a conditional
+     store then.  */
+  if ((!always_stored && !multi_threaded_model_p)
+      || (ref->loaded && bitmap_bit_p (ref->loaded, loop->num)))
     {
       load = gimple_build_assign (tmp_var, unshare_expr (ref->mem.ref));
       lim_data = init_lim_data (load);
-- 
2.21.0.windows.1

