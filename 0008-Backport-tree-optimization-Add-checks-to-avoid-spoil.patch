From 79d1ed2d7f166a498662f6111a4defc55f0061c7 Mon Sep 17 00:00:00 2001
From: yangyang <yangyang305@huawei.com>
Date: Thu, 15 Jul 2021 09:27:27 +0800
Subject: [PATCH 08/13] [Backport]tree-optimization: Add checks to avoid
 spoiling if-conversion

Reference: https://gcc.gnu.org/git/?p=gcc.git;a=commit;h=33d114f570b4a3583421c700396fd5945acebc28

Add some checks in pass_splits_paths, so that pass_split_paths can recognize
the missed if-conversion opportunity and do not duplicate the corresponding
block.

diff --git a/gcc/gimple-ssa-split-paths.c b/gcc/gimple-ssa-split-paths.c
index b3efd43c7ef..9c32da76369 100644
--- a/gcc/gimple-ssa-split-paths.c
+++ b/gcc/gimple-ssa-split-paths.c
@@ -34,6 +34,7 @@ along with GCC; see the file COPYING3.  If not see
 #include "gimple-ssa.h"
 #include "tree-phinodes.h"
 #include "ssa-iterators.h"
+#include "fold-const.h"
 
 /* Given LATCH, the latch block in a loop, see if the shape of the
    path reaching LATCH is suitable for being split by duplication.
@@ -254,6 +255,44 @@ is_feasible_trace (basic_block bb)
 	}
     }
 
+  /* Canonicalize the form.  */
+  if (single_pred_p (pred1) && single_pred (pred1) == pred2
+      && num_stmts_in_pred1 == 0)
+    std::swap (pred1, pred2);
+
+  /* This is meant to catch another kind of cases that are likely opportunities
+     for if-conversion.  After canonicalizing, PRED2 must be an empty block and
+     PRED1 must be the only predecessor of PRED2.  Moreover, PRED1 is supposed
+     to end with a cond_stmt which has the same args with the PHI in BB.  */
+  if (single_pred_p (pred2) && single_pred (pred2) == pred1
+      && num_stmts_in_pred2 == 0)
+    {
+      gimple *cond_stmt = last_stmt (pred1);
+      if (cond_stmt && gimple_code (cond_stmt) == GIMPLE_COND)
+	{
+	  tree lhs = gimple_cond_lhs (cond_stmt);
+	  tree rhs = gimple_cond_rhs (cond_stmt);
+
+	  gimple_stmt_iterator gsi;
+	  for (gsi = gsi_start_phis (bb); !gsi_end_p (gsi); gsi_next (&gsi))
+	    {
+	      gimple *phi = gsi_stmt (gsi);
+	      if ((operand_equal_p (gimple_phi_arg_def (phi, 0), lhs)
+		   && operand_equal_p (gimple_phi_arg_def (phi, 1), rhs))
+		  || (operand_equal_p (gimple_phi_arg_def (phi, 0), rhs)
+		      && (operand_equal_p (gimple_phi_arg_def (phi, 1), lhs))))
+		{
+		  if (dump_file && (dump_flags & TDF_DETAILS))
+		    fprintf (dump_file,
+			     "Block %d appears to be optimized to a join "
+			     "point for if-convertable half-diamond.\n",
+			     bb->index);
+		  return false;
+		}
+	    }
+	}
+    }
+
   /* If the joiner has no PHIs with useful uses there is zero chance
      of CSE/DCE/jump-threading possibilities exposed by duplicating it.  */
   bool found_useful_phi = false;
diff --git a/gcc/testsuite/gcc.dg/tree-ssa/split-path-12.c b/gcc/testsuite/gcc.dg/tree-ssa/split-path-12.c
new file mode 100644
index 00000000000..19a130d9bf1
--- /dev/null
+++ b/gcc/testsuite/gcc.dg/tree-ssa/split-path-12.c
@@ -0,0 +1,19 @@
+/* { dg-do compile } */
+/* { dg-options "-O2 -fsplit-paths -fdump-tree-split-paths-details " } */
+
+double
+foo(double *d1, double *d2, double *d3, int num, double *ip)
+{
+  double dmax[3];
+
+  for (int i = 0; i < num; i++) {
+    dmax[0] = d1[i] < dmax[0] ? dmax[0] : d1[i];
+    dmax[1] = d2[i] < dmax[1] ? dmax[1] : d2[i];
+    dmax[2] = d3[i] < dmax[2] ? dmax[2] : d3[i];
+    ip[i] = dmax[2];
+  }
+
+  return dmax[0] + dmax[1] + dmax[2];
+}
+
+/* { dg-final { scan-tree-dump "appears to be optimized to a join point for if-convertable half-diamond" "split-paths" } } */
-- 
2.21.0.windows.1

